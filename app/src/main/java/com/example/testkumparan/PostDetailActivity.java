package com.example.testkumparan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.testkumparan.Adapter.CommentAdapter;
import com.example.testkumparan.Adapter.PostAdapter;
import com.example.testkumparan.Model.CommentModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostDetailActivity extends AppCompatActivity {

    @BindView(R.id.tv_titleDetail)
    TextView tv_title;
    @BindView(R.id.tv_bodyDetail)
    TextView tv_body;
    @BindView(R.id.tv_usernameDetail)
    TextView tv_username;
    @BindView(R.id.rv_comment)
    RecyclerView recyclerView;

    private int postId,userId;
    private ArrayList<CommentModel> listdetail;
    private CommentAdapter commentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("title");
        String body = bundle.getString("body");
        String username = bundle.getString("username");
        String company = bundle.getString("name");
        postId = bundle.getInt("postId");
        userId = bundle.getInt("userId");
        String email = bundle.getString("email");
        String address = bundle.getString("address");

        tv_title.setText(title);
        tv_body.setText(body);
        tv_username.setText(username);

        getData();

        tv_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), UserDetailActivity.class);
                intent.putExtra("username", tv_username.getText());
                intent.putExtra("name", company);
                intent.putExtra("email", email);
                intent.putExtra("address", address);
                intent.putExtra("userId", userId);
                startActivity(intent);
            }
        });
    }

    private void showComment() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        commentAdapter = new CommentAdapter(listdetail);
        recyclerView.setAdapter(commentAdapter);
    }


    private void getData() {
        listdetail = new ArrayList<>();
        AndroidNetworking.get(BaseUrl.baseUrl + "/comments?postId=" + postId)
                .setTag("test")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject commentObject = response.getJSONObject(i);
                                String name = commentObject.getString("name");
                                String body = commentObject.getString("body");
                                listdetail.add(new CommentModel(name, body));
                                Log.d("name", name);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        showComment();
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}