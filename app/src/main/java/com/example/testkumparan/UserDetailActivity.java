package com.example.testkumparan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.testkumparan.Adapter.AlbumAdapter;
import com.example.testkumparan.Adapter.CommentAdapter;
import com.example.testkumparan.Model.AlbumModel;
import com.example.testkumparan.Model.CommentModel;
import com.example.testkumparan.Model.PostModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator;

public class UserDetailActivity extends AppCompatActivity {

    @BindView(R.id.tv_profileName)
    TextView tv_name;
    @BindView(R.id.tv_profileEmail)
    TextView tv_Email;
    @BindView(R.id.tv_profileAddress)
    TextView tv_address;
    @BindView(R.id.tv_profileCompany)
    TextView tv_company;
    @BindView(R.id.rv_album)
    RecyclerView rv_album;
    @BindView(R.id.indicator_slider_detail)
    ScrollingPagerIndicator indicator;

    private int albumId;
    String userId;
    private ArrayList<AlbumModel> listAlbum;
    private AlbumAdapter albumAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        String username = bundle.getString("username");
        String company = bundle.getString("name");
        String email = bundle.getString("email");
        String address = bundle.getString("address");
        userId = bundle.getString("userId");

        tv_name.setText(username);
        tv_company.setText(company);
        tv_Email.setText(email);
        tv_address.setText(address);

        getData();

    }

    private void getData() {
        listAlbum = new ArrayList<>();
        AndroidNetworking.get(BaseUrl.baseUrl + "/albums?userId="+ userId)
                .setTag("test")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("response", ""+response.length());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject album = response.getJSONObject(i);

                                AlbumModel albumModel = new AlbumModel();
                                albumModel.setAlbumName(album.getString("title"));
                                albumModel.setAlbumId(album.getInt("id"));

                                AndroidNetworking.get(BaseUrl.baseUrl+ "/photos?albumId="+albumId)
                                        .setTag("test")
                                        .setPriority(Priority.LOW)
                                        .build()
                                        .getAsJSONObject(new JSONObjectRequestListener() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                try {
                                                    albumModel.setImage(response.getString("thumbnailUrl"));
                                                    listAlbum.add(albumModel);
                                                    showAlbum();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onError(ANError anError) {

                                            }
                                        });

                            } catch (Exception e) {
                                Log.d("err", "error");
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("err2", "error");
                    }
                });
    }

    private void showAlbum() {
        rv_album.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        albumAdapter = new AlbumAdapter(listAlbum);
        albumAdapter.setOnItemClickListener(new AlbumAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getApplicationContext(), PhotoDetailActivity.class);
                intent.putExtra("title", listAlbum.get(position).getAlbumName());
                startActivity(intent);
            }
        });
        rv_album.setAdapter(albumAdapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rv_album);
        indicator.attachToRecyclerView(rv_album);
        Log.d("array size", "size: " + listAlbum.size());
    }


}