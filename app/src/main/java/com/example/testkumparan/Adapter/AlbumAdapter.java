package com.example.testkumparan.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.testkumparan.Model.AlbumModel;
import com.example.testkumparan.R;

import java.util.ArrayList;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {
    ArrayList<AlbumModel> listalbum;
    private AlbumAdapter.OnItemClickListener mListener;
    public AlbumAdapter(ArrayList<AlbumModel> listalbum){
        this.listalbum = listalbum;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
    public void setOnItemClickListener(AlbumAdapter.OnItemClickListener listener) {
        mListener = listener;
    }

    @Override
    public AlbumAdapter.AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_album, parent, false);
        return new AlbumAdapter.AlbumViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull  AlbumAdapter.AlbumViewHolder holder, int position) {
        AlbumModel albumModel = listalbum.get(position);
        holder.albumName.setText(albumModel.getAlbumName());

        if (!albumModel.getImage().equals("null")) {
            Glide.with(holder.itemView.getContext())
                    .load(albumModel.getImage())
                    .into(holder.album);
        }
    }

    @Override
    public int getItemCount() {
        return listalbum.size();
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder {
        TextView albumName;
        ImageView album;

        public AlbumViewHolder(@NonNull View itemView,final AlbumAdapter.OnItemClickListener listener) {
            super(itemView);
            albumName = itemView.findViewById(R.id.tv_albumName);
            album = itemView.findViewById(R.id.iv_album);

            album.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
