package com.example.testkumparan.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testkumparan.Model.CommentModel;
import com.example.testkumparan.R;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {
    ArrayList<CommentModel> listcomment;

    public CommentAdapter(ArrayList<CommentModel> listcomment){
        this.listcomment = listcomment;
    }

    @Override
    public CommentAdapter.CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_comment, parent, false);
        return new CommentAdapter.CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentAdapter.CommentViewHolder holder, int position) {
        CommentModel commentModel = listcomment.get(position);
        holder.author.setText(commentModel.getAuthor());
        holder.commentBody.setText(commentModel.getBody());
    }

    @Override
    public int getItemCount() {
        return listcomment.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        TextView author,commentBody;
        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            author = itemView.findViewById(R.id.tv_author);
            commentBody = itemView.findViewById(R.id.tv_bodyComment);
        }
    }
}
