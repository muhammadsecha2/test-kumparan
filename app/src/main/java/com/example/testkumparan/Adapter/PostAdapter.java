package com.example.testkumparan.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testkumparan.Model.PostModel;
import com.example.testkumparan.PostDetailActivity;
import com.example.testkumparan.R;

import java.util.ArrayList;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {
    private ArrayList<PostModel> listpost;
    private OnItemClickListener mListener;
    private Context mContext;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public PostAdapter(Context mContext, ArrayList<PostModel> listpost) {
        this.mContext = mContext;
        this.listpost = listpost;
    }


    @Override
    public PostAdapter.PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_post, parent, false);
        return new PostViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull  PostAdapter.PostViewHolder holder, int position) {
        PostModel postModel = listpost.get(position);
        holder.tv_title.setText(postModel.getTitle());
        holder.tv_postBody.setText(postModel.getPostBody());
        holder.tv_username.setText(postModel.getUsername());
        holder.tv_companyName.setText(postModel.getCompanyName());
    }

    @Override
    public int getItemCount() {
        return listpost.size();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title,tv_postBody,tv_username,tv_companyName;
        CardView cd_post;

        public PostViewHolder(@NonNull  View itemView, final OnItemClickListener listener) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_postTitle);
            tv_postBody = itemView.findViewById(R.id.tv_postBody);
            tv_username = itemView.findViewById(R.id.tv_userName);
            tv_companyName = itemView.findViewById(R.id.tv_companyName);
            cd_post = itemView.findViewById(R.id.cd_post);

            cd_post.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
