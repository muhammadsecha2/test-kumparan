package com.example.testkumparan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.testkumparan.Adapter.PostAdapter;
import com.example.testkumparan.Model.PostModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvPosts;
    private PostAdapter postAdapter;
    private ArrayList<PostModel> listPost;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvPosts = findViewById(R.id.rv_post);
        getPost();
    }

    private void showPost() {
        rvPosts.setLayoutManager(new LinearLayoutManager(this));
        postAdapter = new PostAdapter(this, listPost);
        postAdapter.setOnItemClickListener(new PostAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getApplicationContext(), PostDetailActivity.class);
                intent.putExtra("title", listPost.get(position).getTitle());
                intent.putExtra("body", listPost.get(position).getPostBody());
                intent.putExtra("username", listPost.get(position).getUsername());
                intent.putExtra("email", listPost.get(position).getEmail());
                intent.putExtra("address", listPost.get(position).getAddress());
                intent.putExtra("name", listPost.get(position).getCompanyName());
                intent.putExtra("postId", listPost.get(position).getPostId());
                intent.putExtra("userId", listPost.get(position).getUserId());
                startActivity(intent);
            }
        });
        rvPosts.setAdapter(postAdapter);
    }

    private void getPost() {
        listPost = new ArrayList<>();

        AndroidNetworking.get(BaseUrl.baseUrl + "/posts")
                .setTag("test")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject item = response.getJSONObject(i);

                                PostModel postModel = new PostModel();
                                postModel.setTitle(item.getString("title"));
                                postModel.setPostBody(item.getString("body"));
                                postModel.setPostId(item.getInt("id"));
                                postModel.setUserId(item.getInt("userId"));

                                AndroidNetworking.get(BaseUrl.baseUrl + "/users/" + item.getInt("userId"))
                                        .setTag("test")
                                        .setPriority(Priority.LOW)
                                        .build()
                                        .getAsJSONObject(new JSONObjectRequestListener() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                try {
                                                    postModel.setUsername(response.getString("username"));
                                                    postModel.setEmail(response.getString("email"));
                                                    JSONObject address = response.getJSONObject("address");
                                                    postModel.setAddress(address.getString("street"));
                                                    JSONObject company = response.getJSONObject("company");
                                                    postModel.setCompanyName(company.getString("name"));
                                                    listPost.add(postModel);
                                                    showPost();
                                                } catch (Exception e) {
                                                    Toast.makeText(getApplicationContext(), "Error 2", Toast.LENGTH_SHORT).show();

                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onError(ANError anError) {

                                            }
                                        });
                            }
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Error 1", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                        if (anError.getErrorCode() != 0) {
                            Log.d(TAG, "onError errorCode : " + anError.getErrorCode());
                            Log.d(TAG, "onError errorBody : " + anError.getErrorBody());
                            Log.d(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        } else {
                            Log.d(TAG, "onError errorDetail : " + anError.getErrorDetail());
                        }
                    }
                });
    }
}