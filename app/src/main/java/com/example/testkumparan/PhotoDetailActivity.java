package com.example.testkumparan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jsibbold.zoomage.ZoomageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoDetailActivity extends AppCompatActivity {

    @BindView(R.id.myZoomageView)
    ZoomageView zoomageView;
    @BindView(R.id.tv_photoName)
    TextView photoName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);

        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        String photoname = bundle.getString("title");

        photoName.setText(photoname);

        
    }
}